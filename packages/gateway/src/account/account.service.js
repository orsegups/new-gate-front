import feathersSequelize from 'feathers-sequelize';
/**
 * Account Service
 *
 * @name AccountService
 * @memberof module:Account
 * @param {Object} settings
 * @returns {Service}
 */
export default function AccountService(settings) {
  return feathersSequelize(settings);
}
