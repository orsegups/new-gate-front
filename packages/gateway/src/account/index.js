/**
  * Account domain
  *
  * @module Account
  */
import AccountService from './account.service';
import AccountModel from './account.model';
import AccountHooks from './account.hooks';

/**
 * Configure account domain with model, services and hooks
 *
 * @name AccountSetup
 * @memberof module:Account
 * @param {Feathers} app
 */
export default function AccountSetup(app) {
  const model = AccountModel(app);

  // Initialize our service with any options it requires
  const accountService = AccountService({ Model: model });

  app.use('/accounts', accountService);

  // Register hooks
  app.service('accounts').hooks(AccountHooks);
}
