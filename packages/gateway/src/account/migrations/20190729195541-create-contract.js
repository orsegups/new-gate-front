/* eslint-disable no-unused-vars */
module.exports = {
  up: (queryInterface, Sequelize) => queryInterface.createTable('Contracts', {
    id: {
      allowNull: false,
      autoIncrement: true,
      primaryKey: true,
      type: Sequelize.INTEGER,
    },
    contractCode: {
      type: Sequelize.STRING,
    },
    empresaContrato: {
      type: Sequelize.STRING,
    },
    filialContrato: {
      type: Sequelize.STRING,
    },
    isActive: {
      type: Sequelize.BOOLEAN,
    },
  }),
  down: (queryInterface, Sequelize) => queryInterface.dropTable('Contracts'),
};
