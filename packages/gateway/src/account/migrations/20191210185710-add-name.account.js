/* eslint-disable no-unused-vars */
module.exports = {
  up: (QI, Sequelize) => QI.addColumn('Accounts', 'name', {
    type: Sequelize.STRING,
    allowNull: true,
  }),
  down: (QI, Sequelize) => QI.removeColumn('Accounts', 'name'),
};
