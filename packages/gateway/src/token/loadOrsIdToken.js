/* istanbul ignore file */
import axios from 'axios';
import { logger } from '@/logger';

/**
 * Load token from orsegups ID if query with username is given
 *
 * @export
 * @memberof module:TokenHooks
 * @typedef module:TokenHooks.loadOrsIdToken
 * @name loadOrsIdToken
 * @param {*} context
 * @returns {Promise.<Context>} hook context with orsegupsID property
 */
export default function loadOrsIdToken(context) {
  const { app, params, result } = context;
  if (!result && params.query && params.query.username) {
    logger.info('loading ID token for %s', params.query.username);
    const orsegupsId = app.get('orsegupsId');
    const idEndpoint = orsegupsId.uri;
    const { username, password } = params.query;
    const append = `grant_type=password&username=${username}?product=2&password=${password}`;
    const config = {
      auth: {
        username: orsegupsId.username,
        password: orsegupsId.password,
      },
    };
    return axios
      .post(`${idEndpoint}/oauth/token?${append}`, {}, config)
      .then((res) => {
        logger.debug('loaded token', res.data);
        return {
          ...context,
          orsegupsID: res.data,
        };
      })
      .catch((err) => {
        logger.error(
          'error loading ID token for %s, details: %s',
          params.query.username,
          err.response ? err.response.data : '(no response)',
        );
        throw err;
      });
  }

  logger.debug('skipping loadOrsIdToken');
  return Promise.resolve(context);
}
