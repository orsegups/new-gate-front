import { logger } from '@/logger';

/**
 * This middleware does not allow skipAuth comming from client
 * it works with REST and socketio
 */
export function secureSkipAuth(...args) {
  const req = args[0];
  const next = args[args.length - 1];
  logger.debug('securing params.skipAuth');
  req.feathers.skipAuth = false;
  next();
}

/**
 * This hook ensure skipAuth exists in context params,
 * if don't, it creates the param with default true value
 *
 * @param {Context} context
 * @returns {Context} modified context
 */
export function ensureSkipAuth(context) {
  if (typeof context.params.skipAuth === 'undefined') {
    logger.debug('skipAuth in params is not defined, setting to true');
    context.params.skipAuth = true;
  }
  return context;
}
