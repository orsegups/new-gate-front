/* eslint-disable camelcase */
import feathers from '@feathersjs/feathers';
import bcrypt from 'bcrypt';
import { Forbidden } from '@feathersjs/errors';
import loadValidTokens from './loadValidTokens';

describe('loadValidTokens', () => {
  let app;
  beforeEach(() => {
    app = feathers();
  });

  it('throws Forbidden if missing username and password in query', () => {
    expect.assertions(1);
    const context = { app, params: {}, service: jest.fn() };
    return loadValidTokens(context).catch((err) => {
      expect(err).toBeInstanceOf(Forbidden);
    });
  });

  it('load a list of valid tokens in result for a given query', async () => {
    const expectedResult = [
      {
        accessToken: 'foo',
        expiresIn: 900,
        userId: 1,
        createdAt: new Date(),
      },
    ];

    // fake user
    const password = await bcrypt.hash('bar', 10);
    const userService = {
      find: jest.fn(() => Promise.resolve([{
        id: expectedResult[0].userId,
        email: 'foo@bar',
        password,
      }])),
    };
    app.use('/users', userService);

    // fake token
    const tokenService = {
      find: jest.fn(() => Promise.resolve(expectedResult)),
    };
    app.use('/tokens', tokenService);

    const context = {
      app,
      service: app.service('/tokens'),
      params: {
        query: {
          username: 'foo@bar',
          password: 'bar',
        },
      },
    };
    const ctx = await loadValidTokens(context);
    expect(userService.find).toBeCalledWith(
      expect.objectContaining({
        query: expect.objectContaining({
          $or: expect.arrayContaining([
            { email: context.params.query.username },
            { cpfCnpj: context.params.query.username },
            { phone: context.params.query.username },
          ]),
        }),
      }),
    );
    expect(tokenService.find).toBeCalledWith(
      expect.objectContaining({
        query: expect.objectContaining({
          userId: expectedResult[0].userId,
        }),
      }),
    );
    expect(ctx.result).toEqual(expectedResult);
  });

  it('dont load a list of invalid tokens in result for a given query', async () => {
    const createdAt = new Date();
    const expiresIn = 60;
    createdAt.setMilliseconds(
      createdAt.getMilliseconds() - expiresIn * 1000,
    );
    const expectedResult = [
      {
        accessToken: 'foo',
        expiresIn,
        userId: 1,
        createdAt,
      },
    ];

    // fake user
    const password = await bcrypt.hash('bar', 10);
    const userService = {
      find: jest.fn(() => Promise.resolve([{
        id: expectedResult[0].userId,
        email: 'foo@bar',
        password,
      }])),
    };
    app.use('/users', userService);

    // fake token
    const tokenService = {
      find: jest.fn(() => Promise.resolve(expectedResult)),
    };
    app.use('/tokens', tokenService);

    const context = {
      app,
      service: app.service('/tokens'),
      params: {
        query: {
          username: 'foo@bar',
          password: 'bar',
        },
      },
    };
    const ctx = await loadValidTokens(context);
    expect(ctx.result).toBeUndefined();
  });

  it('skip if skipAuth was found', async () => {
    const context = {
      app,
      params: { skipAuth: true },
    };
    const res = await loadValidTokens(context);
    expect(res).toBe(context);
  });
});
