/* eslint-disable no-unused-vars */
module.exports = {
  up: (QI, Sequelize) => QI.addColumn('Tokens', 'userId', {
    type: Sequelize.INTEGER,
    references: {
      model: 'Users',
      key: 'id',
    },
    allowNull: false,
  }),
  down: (QI, Sequelize) => QI.removeColumn('Tokens', 'userId'),
};
