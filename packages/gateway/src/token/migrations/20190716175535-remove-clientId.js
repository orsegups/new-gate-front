/* eslint-disable no-unused-vars */
module.exports = {
  up: (QI, Sequelize) => QI.removeColumn('Tokens', 'clienteId'),
  down: (QI, Sequelize) => QI.addColumn('Tokens', 'clienteId', {
    type: Sequelize.INTEGER,
  }),
};
