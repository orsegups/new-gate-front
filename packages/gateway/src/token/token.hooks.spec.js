/* eslint-disable global-require */
import loadValidTokens from './loadValidTokens';
import loadOrsIdToken from './loadOrsIdToken';
import loadOrsIdAccounts from './loadOrsIdAccounts';
import persistOrsId from './persistOrsId';
import clientToken from './clientToken';
import { globalHooks } from './token.hooks';

jest.mock('@/lib/hooks/allowMethods', () => ({
  __esModule: true,
  default: (...args) => args,
}));

jest.mock('./clientToken');

describe('token hooks', () => {
  describe('local service hooks', () => {
    it('allow only find method in rest and socketio', () => {
      const hooks = require('./token.hooks').default;
      expect(hooks.before.all).toEqual([
        ['rest', ['find']],
        ['socketio', ['find']],
      ]);
    });

    it('add find hooks', () => {
      const hooks = require('./token.hooks').default;
      expect(hooks.before.find).toEqual([
        loadValidTokens,
        loadOrsIdToken,
        loadOrsIdAccounts,
        persistOrsId,
      ]);
    });
  });

  describe('global   hooks', () => {
    it('add before all hooks', () => {
      expect(clientToken).toBeCalledWith({
        bypassServices: [
          'tokens', 'users/passwordResets', 'users/passwordChange', 'hello',
        ],
      });
      expect(globalHooks).toMatchObject({
        before: {
          all: expect.any(Array),
        },
      });
    });
  });
});
