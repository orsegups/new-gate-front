/**
  * Token domain
  *
  * @module Token
  */
import TokenService from './token.service';
import TokenModel from './token.model';
import TokenHooks, { globalHooks } from './token.hooks';
import setupServerToken from './serverToken';

/**
 * Configure token domain with model, services and hooks
 *
 * @name TokenSetup
 * @memberof module:Token
 * @param {Feathers} app
 */
export default function TokenSetup(app) {
  const model = TokenModel(app);

  // Initialize our service with any options it requires
  const tokenService = TokenService({ Model: model });

  // Register tokenService
  app.use('/tokens', tokenService);

  // Register hooks
  app.service('tokens').hooks(TokenHooks);

  // Register global hooks
  app.hooks(globalHooks);

  // Setup server token
  setupServerToken(app);
}
