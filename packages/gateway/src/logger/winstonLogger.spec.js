import logger from './winstonLogger';

describe('logger winstonLogger', () => {
  it('configures the Winston logger', () => {
    expect(logger).toMatchObject({
      level: 'info',
    });
  });
});
