import swagger from 'feathers-swagger';
import swaggerUi from 'swagger-ui-express';

import feathers from '@feathersjs/feathers';
import configuration from '@feathersjs/configuration';

import setupSwagger from './swagger';

jest.mock('feathers-swagger');
jest.mock('swagger-ui-express');

describe('swagger', () => {
  let app;

  beforeAll(() => {
    jest.setTimeout(30000);
  });

  beforeEach(() => {
    app = feathers();
    app.configure(configuration());
  });

  it('configure swagger', (done) => {
    process.env.APP_VERSION = 'foo';
    swagger.mockImplementation(settings => () => {
      expect(settings).toEqual(
        {
          specs: {
            info: {
              title: expect.any(String),
              description: expect.any(String),
              version: process.env.APP_VERSION,
            },
          },
        },
      );
      done();
    });
    app.configure(setupSwagger);
  });

  it('sets a middleware on app', () => {
    app.use = jest.fn();
    swagger.mockImplementation(() => () => {});
    const uri = `${app.get('host')}:${app.get('port')}`;
    swaggerUi.setup.mockImplementation(() => 'setupUi');
    app.configure(setupSwagger);
    expect(swaggerUi.setup).toBeCalledWith(
      null,
      {
        swaggerOptions: {
          url: `http://${uri}/docs`,
        },
      },
    );
    expect(app.use).toBeCalledWith(
      '/api-docs',
      swaggerUi.serve,
      'setupUi',
    );
  });

  it('sets a middleware on app for staging', () => {
    const oldEnv = process.env.NODE_ENV;
    process.env.NODE_ENV = 'staging';
    app.use = jest.fn();
    swagger.mockImplementation(() => () => { });
    const uri = app.get('host');
    swaggerUi.setup.mockImplementation(() => 'setupUi');
    app.configure(setupSwagger);
    expect(swaggerUi.setup).toBeCalledWith(
      null,
      {
        swaggerOptions: {
          url: `http://${uri}/docs`,
        },
      },
    );
    process.env.NODE_ENV = oldEnv;
  });
});
