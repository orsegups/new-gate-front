import Sequelize from 'sequelize';
import { logger } from '@/logger';

// FIXME: improve test coverage removing ignore comments
export default function setupDatabase(app) {
  const db = app.get('database');
  /* istanbul ignore next */
  const database = process.env.MYSQL_DATABASE || db.database;
  /* istanbul ignore next */
  const username = process.env.MYSQL_USER || db.username;
  /* istanbul ignore next */
  const password = process.env.MYSQL_PASSWORD || db.password;
  /* istanbul ignore next */
  const host = process.env.MYSQL_HOST || db.host;
  /* istanbul ignore next */
  const dialect = db.dialect || 'mysql';
  /* istanbul ignore next */
  const prefix = db.prefix || '';

  app.db = new Sequelize(database, username, password, {
    host,
    dialect,
    hooks: {
      beforeDefine(columns, model) {
        model.tableName = `${prefix}${model.name.plural}`;
        // model.schema = 'public';
      },
    },
  });

  app.db.setupAssociations = () => {
    const { models } = app.db;
    Object.keys(models).forEach((modelName) => {
      if (models[modelName].associate) {
        logger.debug('setting associations for %s model', modelName);
        models[modelName].associate(models);
      }
    });
  };
}
