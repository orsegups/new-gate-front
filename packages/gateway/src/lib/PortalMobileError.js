/* istanbul ignore file */
const { FeathersError } = require('@feathersjs/errors');

export default class PortalMobileError extends FeathersError {
  constructor(pmError, data) {
    super(
      `Error '${pmError.msg}' when requesting Portal Mobile API`,
      'portal-mobile-error',
      502,
      'PortalMobileError',
      { ...data, pmError },
    );
  }
}
