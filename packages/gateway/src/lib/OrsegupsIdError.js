/* istanbul ignore file */
const { FeathersError } = require('@feathersjs/errors');

export default class OrsegupsIdError extends FeathersError {
  constructor(axiosError, data = {}) {
    const idError = axiosError.response.data;
    super(
      `Error ${idError.error} (${idError.message}) when requesting Orsegups ID`,
      'orsegups-id-error',
      502,
      'OrsegupsIdError',
      { idError, ...data },
    );

    this.config = axiosError.config;
    this.request = axiosError.request;
    this.response = axiosError.response;
    this.idError = idError;
  }
}
