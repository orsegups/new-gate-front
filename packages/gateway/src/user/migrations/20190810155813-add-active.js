/* eslint-disable no-unused-vars */
module.exports = {
  up: (QI, Sequelize) => QI.addColumn('Users', 'active', {
    type: Sequelize.BOOLEAN,
    allowNull: false,
  }),
  down: (QI, Sequelize) => QI.removeColumn('Users', 'active'),
};
