/* eslint-disable no-unused-vars */


module.exports = {
  up: (QI, Sequelize) => QI.sequelize.query(
    'ALTER TABLE Tokens ADD CONSTRAINT Tokens_userId_foreign_idx FOREIGN KEY(userId) REFERENCES Users(id);',
  ),
  down: (QI, Sequelize) => QI.sequelize.query(
    'ALTER TABLE Tokens DROP FOREIGN KEY Tokens_userId_foreign_idx;',
  ),
};
