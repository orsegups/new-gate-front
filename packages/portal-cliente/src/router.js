/* eslint-disable max-len */
/**
 * Rotas da aplicação
 *
 * |Rota                                        | Descrição                               | View                 |
 * |--------------------------------------------|-----------------------------------------|----------------------|
 * | /                                          | Gráficos e estatísticas                 | Dashboard            |
 * | /login                                     | Autenticação                            | Login                |
 * | /recover                                   | Recuperaçãoo de senha                   | Recover              |
 * | /convidados                                | Gestão de convidados                    | GuestsList           |
 * | /convidados/adicionar                      | Adição de convidados                    | GuestsUpsert         |
 * | /convidados/:id/atualizar                  | Atualização de convidados               | GuestsUpsert         |
 * | /perfil                                    | Gestão do perfil do usuário logado      | Profile              |
 * | /boletos-e-notas                           | Gestão de boletos e notas               | Invoices             |
 * | /documentos                                | Gestão de documentos                    | Documents            |
 * | /termo-de-aceite                           | Termo de Aceite                         | Terms                |
 * | /seguranca-eletronica                      | Eventos WebAlarm e Ordem de serviços    | AlarmsList           |
 * | /seguranca-eletronica/:id/ordem-de-servico | Ordem de serviço                        | AlarmsServiceOrders  |
 * | /minhas-solicitacoes                       | Visualizar solicitações                 | ContactsList         |
 * | /minhas-solicitacoes/adicionar             | Adicionar nova solicitação              | ContactsAdd          |
 * | /minhas-solicitacoes/:id/histórico         | Visualizar histórico de uma solicitacao | ContactsView         |
 * | /logout                                    | Sair                                    | Logout               |
 * | /blank                                     | Página vazia                            | Blank             |
 * | /notifications                             | Notificações e gestão de Rats           | Notifications             |
 *
 * @module router
 */
/* eslint-enable max-len */
import Vue from 'vue';
import Router from 'vue-router';
import Login from './views/Login.vue';
import Recover from './views/Recover.vue';

Vue.use(Router);

export default new Router({
  mode: 'history',
  base: process.env.BASE_URL,
  scrollBehavior: () => ({ x: 0, y: 0 }),
  routes: [
    {
      path: '/',
      name: 'Dashboard',
      component: () => import(/* webpackChunkName: "Dashboard" */ './views/Dashboard.vue'),
      meta: { requiresAuth: true },
    },
    {
      path: '/login',
      name: 'Login',
      component: Login,
    },
    {
      path: '/recover',
      name: 'Recover',
      component: Recover,
    },
    {
      path: '/about',
      name: 'About',
      // route level code-splitting
      // this generates a separate chunk (about.[hash].js) for this route
      // which is lazy-loaded when the route is visited.
      component: () => import(/* webpackChunkName: "About" */ './views/About.vue'),
    },
    {
      path: '/termo-de-aceite',
      name: 'Terms',
      props: true,
      component: () => import(/* webpackChunkName: "Terms" */ './views/Terms.vue'),
    },
    {
      path: '/convidados/',
      name: 'GuestsList',
      meta: { requiresAuth: true },
      // route level code-splitting
      // this generates a separate chunk (about.[hash].js) for this route
      // which is lazy-loaded when the route is visited.
      component: () => import(/* webpackChunkName: "GuestsList" */ './views/GuestsList.vue'),
    },
    {
      path: '/convidados/adicionar',
      name: 'GuestsAdd',
      meta: { requiresAuth: true },
      // route level code-splitting
      // this generates a separate chunk (about.[hash].js) for this route
      // which is lazy-loaded when the route is visited.
      component: () => import(/* webpackChunkName: "GuestsAdd" */ './views/GuestsUpsert.vue'),
    },
    {
      path: '/convidados/:id/atualizar',
      name: 'GuestsUpdate',
      props: true,
      meta: { requiresAuth: true },
      // route level code-splitting
      // this generates a separate chunk (about.[hash].js) for this route
      // which is lazy-loaded when the route is visited.
      component: () => import(/* webpackChunkName: "GuestsUpdate" */ './views/GuestsUpsert.vue'),
    },
    {
      path: '/perfil',
      name: 'Profile',
      meta: { requiresAuth: true },
      // route level code-splitting
      // this generates a separate chunk (about.[hash].js) for this route
      // which is lazy-loaded when the route is visited.
      component: () => import(/* webpackChunkName: "Profile" */ './views/Profile.vue'),
    },
    {
      path: '/minhas-solicitações',
      name: 'ContactsList',
      meta: { requiresAuth: true },
      component: () => import(/* webpackChunkName: "ContactsList" */ './views/ContactsList.vue'),
      children: [
        {
          path: 'adicionar',
          name: 'ContactsAdd',
          component: () => import(/* webpackChunkName: "ContactsAdd" */ './views/ContactsAdd.vue'),
        }, {
          path: ':id/histórico',
          name: 'ContactsView',
          props: true,
          component: () => import(/* webpackChunkName: "ContactsView" */ './views/ContactsView.vue'),
        },
      ],
    },
    {
      path: '/seguranca-eletronica',
      name: 'AlarmsList',
      meta: { requiresAuth: true },
      component: () => import(/* webpackChunkName: "AlarmsList" */ './views/AlarmsList.vue'),
      children: [
        {
          path: ':alarmIdx/ordem-de-servico',
          name: 'AlarmsServiceOrders',
          props: true,
          component: () => import(/* webpackChunkName: "AlarmsServiceOrders" */ './views/AlarmsServiceOrders.vue'),
        },
      ],
    },
    {
      path: '/boletos-e-notas',
      name: 'invoices',
      meta: { requiresAuth: true },
      component: () => import(/* webpackChunkName: "invoices" */ './views/Invoices.vue'),
    },
    {
      path: '/documentos',
      name: 'Documents',
      meta: { requiresAuth: true },
      component: () => import(/* webpackChunkName: "documents" */ './views/Documents.vue'),
    },
    {
      path: '/blank',
      name: 'Blank',
      meta: { requiresAuth: true },
      component: () => import(/* webpackChunkName: "documents" */ './views/Blank.vue'),
    },
    {
      path: '/notificações',
      name: 'Notifications',
      meta: { requiresAuth: true },
      component: () => import(/* webpackChunkName: "notifications" */ './views/Notifications.vue'),
    },
  ],
});
