import Vue from 'vue';
import Vuetify from 'vuetify/lib';

// for v-lazy
import 'intersection-observer';

import '@mdi/font/css/materialdesignicons.css';

Vue.use(Vuetify);

export default new Vuetify({
  icons: {
    iconfont: 'mdi',
  },
});
