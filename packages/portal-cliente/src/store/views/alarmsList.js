/* istanbul ignore file */
const state = {
  alarms: null,
  serviceOrders: null,
};
const mutations = {
  setAlarms(st, alarms) {
    st.alarms = alarms;
  },
  setServiceOrders(st, orders) {
    st.serviceOrders = orders;
  },
};
const getters = {
  isLoading(st) {
    return !st.alarms;
  },
  noResults(st) {
    return st.alarms && st.alarms.length === 0;
  },
};
const actions = {
  async loadAlarms({ commit, dispatch, rootState }) {
    commit('setAlarms', null); // to enable loading

    const url = '/portalcliente/v1/minhasegurancaeletronica';
    const { cpfCnpjOwner, contractCode } = rootState.session.currentAccount;
    const params = {
      cpfCnpj: cpfCnpjOwner,
      contractCode,
    };
    const method = 'get';
    const onError = () => {
      this.$notify('Erro ao carregar alarmes.');
    };
    try {
      const res = await dispatch('mobileRequests/create', { url, params, method }, { root: true });
      if (res.data.status === 100 || res.data.status === 101) {
        commit('setAlarms', res.data.ret);
      } else {
        onError();
      }
    } catch (e) {
      onError(e);
    }
  },
  loadServiceOrder({ state: st, commit }, alarmIdx) {
    const aIdx = Number(alarmIdx);
    // will always be executed with alarms in context
    if (st.alarms && st.alarms[aIdx]) {
      const serviceOrders = st.alarms[aIdx].ordensDeServico;
      commit('setServiceOrders', serviceOrders);
    }
  },
};
export default {
  namespaced: true,
  state,
  getters,
  mutations,
  actions,
};
