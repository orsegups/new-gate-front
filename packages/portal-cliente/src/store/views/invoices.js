/* istanbul ignore file */
import adblockDetect from 'adblock-detect';
import b64ToBlob from '@/lib/b64toBlob';

const state = {
  periods: null,
  noMoreResults: false,
  activeTooltipInvoice: null,
};
const mutations = {
  setPeriods(st, periods) {
    st.periods = periods;
  },
  setNoMoreResults(st, noMore) {
    st.noMoreResults = noMore;
  },
  setActiveTooltipInvoice(st, { invoice, active }) {
    if (active) {
      st.activeTooltipInvoice = invoice;
    } else {
      st.activeTooltipInvoice = null;
    }
  },
};
const getters = {
  isLoading(st) {
    return !st.periods;
  },
  noResults(st, gt) {
    return gt.periods && gt.periods.length === 0;
  },
  isInvoiceTooltipVisible(st) {
    return invoice => st.activeTooltipInvoice === invoice;
  },
};
const actions = {
  async loadPeriods({
    // eslint-disable-next-line no-unused-vars
    commit, dispatch, rootState, state: st,
  }, { qpt = new Date(), all = false } = {}) {
    commit('setPeriods', null); // to enable loading

    const url = '/portalcliente/v1/competenciaNotas';
    const { cpfCnpjOwner } = rootState.session.currentAccount;

    const month = `${qpt.getMonth() + 1}`;
    // eslint-disable-next-line no-unused-vars
    const qptString = `${month.padStart(2, '0')}/${qpt.getFullYear()}`;

    const params = {
      cgcCpf: cpfCnpjOwner,
      // cpt: qptString,
    };
    const method = 'get';
    const onError = () => {
      this.$notify('Não Foram encontradas Notas no período de 6 meses.');
    };
    try {
      const res = await dispatch('mobileRequests/create', { url, params, method }, { root: true });
      if (res.data.status === 100) {
        // FIXME: assuming services[0]
        const services = res.data.ret;
        const periods = [
          // TODO: add competence pagination
          // ...(st.periods || []),
          ...services[0].competenciaDocFinanceiroSite,
        ];
        const sortedPeriods = periods.sort((prev, next) => {
          const pMonth = Number(prev.competencia.substr(0, 2));
          const pYear = Number(prev.competencia.substr(3));
          const nMonth = Number(next.competencia.substr(0, 2));
          const nYear = Number(prev.competencia.substr(3));
          return (pMonth + pYear * 12) - (nMonth + nYear * 12) * -1;
        });

        const end = all ? sortedPeriods.length : 4;
        if (all) commit('setNoMoreResults', true);
        const slicedPeriods = sortedPeriods.slice(0, end);

        // store deep invoices in localStorage to reduce RAM usage
        const transformedPeriods = slicedPeriods.map((period) => {
          const invoices = [];
          const docs = period.documentoFinanceiroSiteDTO || [];
          docs.forEach((doc) => {
            const loader = () => doc;
            invoices.push(loader);
          });

          return {
            ...period,
            documentoFinanceiroSiteDTO: undefined,
            invoices,
          };
        });
        commit('setPeriods', transformedPeriods);
      } else {
        //onError();
      }
    } catch (e) {
      //onError(e);
    }
  },
  // TODO: add server pagination
  // loadMorePeriods({ dispatch, commit, state: st }) {
  //   const newQpt = new Date(st.qpt.getTime());
  //   newQpt.setMonth(newQpt.getMonth() + 3);
  //   dispatch('loadPeriods', newQpt);
  // },
  async loadInvoiceFile({ dispatch }, { link, invoice, field }) {
    const adblock = await new Promise((resolve) => {
      adblockDetect(resolve);
    });
    if (adblock) {
      this.$notify(
        'Detectamos que você está usando uma extensão Adblock, caso não seja possível baixar o arquivo, por favor desabilite a extensão e tente novamente.',
        'info',
      );
    }
    const fileWindow = window.open();
    try {
      const res = await dispatch('fileRequests/create', { url: link }, { root: true });
      const blob = b64ToBlob(res.base64, res.headers['content-type']);
      const url = fileWindow.URL.createObjectURL(blob);
      if (field === 'linkXML') {
        const a = fileWindow.document.createElement('a');
        const getFileName = (cd = '') => {
          const regex = /Filename="(.*)"/gm;
          const matches = regex.exec(cd);
          if (matches[1]) return matches[1];
          return `${res.id}.xml`;
        };
        a.href = url;
        a.target = '_blank';
        a.download = getFileName(res.headers['content-disposition']);
        a.onclick = () => {
          fileWindow.close();
        };
        fileWindow.document.body.appendChild(a);
        a.click();
      } else {
        fileWindow.location = url;
      }
    } catch (e) {
      if (field === 'linkNfseDownload') {
        fileWindow.location = invoice.linkNfse;
      } else {
        fileWindow.close();
        this.$notify(
          'Desculpe , não existe nenhum documento associado a esse tipo de arquivo no momento,  dúvidas contatar o nosso atendimento ao cliente.',
        );
        // TODO: log this event
        // throw e;
      }
    }
  },
};
export default {
  namespaced: true,
  state,
  getters,
  mutations,
  actions,
};
