const state = {
  currentUser: null,
};
const mutations = {
  setUser(st, user) {
    st.currentUser = user;
  },
};
const getters = {
  currentUserId(st, gt, rootState) {
    const { token } = rootState.session;
    return token && token.userId;
  },
  currentUser(st) {
    return st.currentUser;
  },
};
const actions = {
  async findCurrentUser({
    dispatch, getters: gt, rootState, commit,
  }) {
    await dispatch('users/get', gt.currentUserId, { root: true });
    commit('setUser', rootState.users.copy);
  },
  async updateUser({ dispatch, getters: gt }, formData) {
    try {
      await dispatch('users/patch', [gt.currentUserId, formData], { root: true });
      this.$notify('Informações do perfil atualizadas com sucesso', 'success');
    } catch (e) {
      this.$notify('Erro ao atualizar perfil');
    }
  },
};
export default {
  namespaced: true,
  state,
  mutations,
  getters,
  actions,
};
