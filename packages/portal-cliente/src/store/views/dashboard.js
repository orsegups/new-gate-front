import joinFields from '@/lib/joinFields';

/* istanbul ignore file */
const state = {
  firstAccess: false,
  statistics: null,
  filteredStatistics: null,
  searchInput: null,
  chartModalData: null,
};
const mutations = {
  setFirstAccess(st, firstAccess) {
    st.firstAccess = firstAccess;
  },
  setStatistics(st, statistics) {
    st.statistics = statistics;
  },
  setFilteredStatistics(st, statistics) {
    st.filteredStatistics = statistics;
  },
  setSearchInput(st, item) {
    st.searchInput = item;
  },
  setChartModalData(st, data) {
    st.chartModalData = data;
  },
};
const getters = {
  currentUserId(st, gt, rootState) {
    const { token } = rootState.session;
    return token && token.userId;
  },
  isPasswordDialogOpen(st) {
    return st.firstAccess;
  },
  searchItems(st) {
    if (st.statistics) {
      const genLocals = stats => stats.reduce((res, stat) => {
        const {
          uf, cidade, bairro, endereco,
        } = stat;
        const fields = {
          endereco,
          bairro,
          cidade,
          uf,
        };
        const label = joinFields(fields);
        res.push({ fields, label });
        return res;
      }, []);
      // TODO: generate terms for tech search like genLocals
      const genTechs = () => ([]);
      const uniq = (arr) => {
        const seen = {};
        return arr.filter((item) => {
          if (seen[item.label]) return false;
          seen[item.label] = true;
          return true;
        });
      };
      const locals = genLocals(st.statistics);
      const techs = genTechs(st.statistics);
      const merged = [
        ...locals,
        ...techs,
      ];

      return uniq(merged);
    }
    return [];
  },
  searchResults(st) {
    if (st.filteredStatistics) {
      return st.filteredStatistics.reduce((charts, statistic, statisticIdx) => {
        const newCharts = statistic.graficos.map(chart => ({
          chart,
          statisticIdx,
        }));
        return [
          ...charts,
          ...newCharts,
        ];
      }, []);
    }
    return [];
  },
  isDialogOpen(st) {
    if (st.chartModalData) return true;
    return false;
  },
};
const actions = {
  async checkUserFirstLogin({
    dispatch, getters: gt, rootState, commit,
  }) {
    await dispatch('users/get', gt.currentUserId, { root: true });
    const { firstAccess } = rootState.users.copy;
    if (firstAccess) commit('setFirstAccess', true);
  },
  async changeUserPassword({ commit }) {
    // TODO: implement changeUserPassword
    // eslint-disable-next-line no-console
    console.log('TODO: implement changeUserPassword');
    commit('setFirstAccess', false);
  },
  async loadStatistics({ dispatch, rootState, commit }, { from, goToInvoices }) {
    const url = '/portalcliente/v1/estatisticas';
    const { cpfCnpjOwner, contractCode } = rootState.session.currentAccount;
    const params = {
      cgcCpf: cpfCnpjOwner,
      contractCode,
    };
    const method = 'get';
    const onError = () => {
      this.$notify('Erro ao carregar as estatísticas.');
    };

    // set loading
    commit('setStatistics', null);
    commit('setFilteredStatistics', null);
    commit('setSearchInput', null);

    const setData = (data = []) => {
      commit('setStatistics', data);
      commit('setFilteredStatistics', data);
    };

    try {
      const res = await dispatch('mobileRequests/create', { url, params, method }, { root: true });
      if (res.data === '') {
        setData();
      } else if (res.data.status === 100) {
        if (res.data.ret && res.data.ret.length === 0 && from && from.name === 'Login') {
          goToInvoices();
        } else {
          // fix: backend is sending two objects duplicated inside the array,
          // and we only need the first one.
          // if backend is fixed, this implementation should still work
          res.data.ret.splice(1);
          setData(res.data.ret);
        }
      } else {
        onError();
      }
    } catch (e) {
      onError(e);
    }
  },
  search({ commit, state: st, getters: gt }, term) {
    const doSearch = (fields, data, orLogic = false) => {
      const isMatching = (v1, v2) => (new RegExp(v2, 'i')).test(v1);
      const finder = orLogic ? 'some' : 'every';
      const filter = stat => Object.keys(fields)[finder](
        // all fields must be equal
        field => isMatching(stat[field], fields[field]),
      );
      return data.filter(filter);
    };
    if (term) {
      const searchItem = gt.searchItems.find(
        item => item.label === term,
      );
      const orLogic = !searchItem;
      const fields = (searchItem && searchItem.fields) || {
        bairro: term,
        cdCliente: term,
        central: term,
        centralPartical: term,
        cidade: term,
        empresa: term,
        endereco: term,
        fantasia: term,
        particao: term,
        razao: term,
        uf: term,
      };
      commit('setSearchInput', term);
      commit('setFilteredStatistics', doSearch(fields, st.statistics, orLogic));
    } else {
      commit('setFilteredStatistics', st.statistics);
      commit('setSearchInput', null);
    }
  },
  openChartModal({ commit }, { res, pon }) {
    const statistic = state.statistics[res.statisticIdx];
    commit('setChartModalData', { statistic, pon });
  },
};
export default {
  namespaced: true,
  state,
  mutations,
  getters,
  actions,
};
