import sessionModule from './session.module';
import { addAccessToken } from './session.hooks';

export default function createSession(router, feathersClient, loginRouteName = 'Login') {
  return function sessionVuexPlugin(store) {
    // Add route guard
    router.beforeEach(async (to, from, next) => {
      const nextParams = {};
      const toLogin = () => {
        nextParams.name = loginRouteName;
        nextParams.query = { redirect: to.fullPath };
      };
      if (to.matched.some(record => record.meta.requiresAuth)) {
        // this route requires auth
        await store.dispatch('session/loadFromStorage');
        if (store.getters['session/loggedIn']) {
          try {
            // load user
            await store.dispatch('session/loadUser');
            // load accounts
            await store.dispatch('session/loadUserAccounts');
          } catch (e) {
            // something gone wrong, redirects to login, show error
            toLogin();
            store.$notify(e);
          }
        } else {
          // not loggedIn, redirects to login
          toLogin();
        }
      }
      next(nextParams); // make sure to always call next()!
    });

    // Add token to all gateway requisitions
    feathersClient.hooks({
      before: {
        all: [addAccessToken(store)],
      },
    });

    store.registerModule('session', sessionModule);
  };
}
