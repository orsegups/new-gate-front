import axios from 'axios';
import OrsegupsID from '@/services/orsegupsID';

jest.mock('axios');

describe('orsegupsID service', () => {
  it('exports an object', () => {
    expect(OrsegupsID).toBeDefined();
  });

  it('get orsegupsID api root', async () => {
    const url = process.env.VUE_APP_ORSEGUPSID_URI;
    const result = {};
    axios.get.mockResolvedValue(result);
    const res = await OrsegupsID.get();
    expect(res).toEqual(result);
    expect(axios.get).toHaveBeenCalledWith(url);
  });
});
