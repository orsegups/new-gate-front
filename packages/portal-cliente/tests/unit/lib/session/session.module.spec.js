import localforage from 'localforage';
import session from '@/lib/session/session.module';
import { getStore } from '~/tests/utils';

function getTokens(token) {
  return {
    namespaced: true,
    getters: {
      list: () => [token],
    },
  };
}

jest.mock('localforage');

describe('session store module', () => {
  beforeEach(() => {
    localforage.setItem.mockReset();
    localforage.getItem.mockReset();
  });

  describe('sucessful session creation', () => {
    it('set state and storage token', async () => {
      const token = {
        id: 1,
        accessToken: 'foo',
        expiresIn: 9999,
        createdAt: new Date(),
      };

      // Fake tokens module
      const tokens = getTokens(token);
      const store = getStore({ session, tokens });

      // create a new session
      await store.dispatch('session/create', { token });
      expect(localforage.setItem).toHaveBeenCalledWith('sessionToken', token);
      expect(store.state.session.token).toBe(token);
      expect(store.getters['session/loggedIn']).toBeTruthy();
      expect(store.getters['session/isTokenExpired']).toBeFalsy();
    });

    it('set available tokens and account from localStorage into state', async () => {
      const token = {
        id: 1,
        accessToken: 'foo',
      };
      const account = {};
      const store = getStore({ session });
      localforage.getItem = jest.fn((key) => {
        if (key === 'sessionAccount') return account;
        if (key === 'sessionToken') return token;
        return null;
      });

      await store.dispatch('session/loadFromStorage');
      expect(localforage.getItem).toHaveBeenNthCalledWith(1, 'sessionToken');
      expect(localforage.getItem).toHaveBeenNthCalledWith(2, 'sessionAccount');
      expect(store.state.session.token).toBe(token);
      expect(store.state.session.currentAccount).toBe(account);
    });

    it('does not set tokens or account in state if localStorage is empty', async () => {
      const store = getStore({ session });
      localforage.getItem = jest.fn();

      await store.dispatch('session/loadFromStorage');
      expect(store.state.session.token).toBeNull();
      expect(store.state.session.currentAccount).toBeNull();
    });

    it('load user accounts and set current account to first available', async () => {
      const userId = 1;
      const user = { id: userId };
      const userAccount = {
        id: 1,
        userOwnerId: userId,
      };
      const users = {
        namespaced: true,
        actions: {
          get: jest.fn(() => Promise.resolve(user)),
        },
      };
      const accounts = {
        namespaced: true,
        actions: {
          find: jest.fn(() => Promise.resolve([userAccount])),
        },
      };
      const token = { userId };
      const store = getStore({ session, accounts, users });
      store.commit('session/setToken', token);
      store.commit('session/setCurrentUser', user);
      await store.dispatch('session/loadUserAccounts');
      expect(accounts.actions.find).toHaveBeenNthCalledWith(
        1,
        expect.any(Object), // context
        {
          query: {
            userOwnerId: userId,
          },
        },
        undefined,
      );
      expect(accounts.actions.find).toHaveBeenNthCalledWith(
        2,
        expect.any(Object), // context
        {
          query: {
            guestId: userId,
          },
        },
        undefined,
      );
      expect(store.state.session.userAccounts).toEqual(
        [userAccount, userAccount], // in server accounts will be different
      );
      expect(store.getters['session/accounts']).toEqual(
        [userAccount, userAccount], // in server accounts will be different
      );
      expect(store.state.session.currentAccount).toEqual(userAccount);
    });

    it('fires accountChangeListeners when current account is changed', () => {
      const store = getStore({ session });
      const listener = jest.fn();
      store.commit('session/addAccountChangeListener', listener);
      store.commit('session/setCurrentAccount', {});
      expect(listener).toBeCalled();

      listener.mockClear();
      store.commit('session/removeAccountChangeListener', listener);
      store.commit('session/setCurrentAccount', {});
      expect(listener).not.toBeCalled();
    });

    it('does not update currentAccount if the storage account is valid', async () => {
      const userId = 1;
      const user = { id: userId };
      const userAccount = {
        id: 1,
        userOwnerId: userId,
      };
      const accounts = {
        namespaced: true,
        actions: {
          find: jest.fn(() => Promise.resolve([userAccount])),
        },
      };
      const token = { userId };

      localforage.setItem.mockClear();
      const store = getStore({ session, accounts });
      store.commit('session/setToken', token);
      store.commit('session/setCurrentUser', user);
      store.commit('session/setCurrentAccount', userAccount);
      await store.dispatch('session/loadUserAccounts');
      expect(store.state.session.currentAccount).toBe(userAccount);
      expect(localforage.setItem).not.toBeCalled();
    });
  });

  describe('failed session creation', () => {
    it('set token state but inform through loggedIn when token is expired', async () => {
      const token = {
        id: 1,
        accessToken: 'foo',
        expiresIn: 0,
        createdAt: new Date(),
      };

      // Fake tokens module
      const tokens = getTokens(token);
      const store = getStore({ session, tokens });

      await store.dispatch('session/create', { token });
      expect(store.state.session.token).toBe(token);
      expect(localforage.setItem).toBeCalledWith('sessionToken', token);
      expect(store.getters['session/loggedIn']).toBeFalsy();
      expect(store.getters['session/isTokenExpired']).toBeTruthy();
    });
  });

  it('removes a session', async () => {
    const token = {
      id: 1,
      accessToken: 'foo',
    };
    const store = getStore({ session });
    store.commit('session/setToken', token);
    await store.dispatch('session/remove');
    expect(localforage.removeItem).toBeCalledWith('sessionToken');
    expect(store.state.session.token).toBeNull();
  });
});
