#!/bin/bash

# THIS SCRIPT WILL RUN ON CI SERVER

PKG=$1
ENV=$2
APP=$3

echo "CURRENT APP: " $APP
echo "CURRENT PACKAGE: " $PKG

export NODE_ENV=${CI_ENVIRONMENT_NAME/"${APP}_"/""}

echo "CURRENT NODE_ENV: $NODE_ENV"

echo "TARGET ENV:" $ENV

echo "PREPARING node_modules"
npx yarn install --frozen-lockfile --production=true

set -e
mkdir ~/.aws/
touch ~/.aws/credentials
printf "[eb-cli]\naws_access_key_id = %s\naws_secret_access_key = %s\n" "$AWS_ACCESS_KEY_ID" "$AWS_SECRET_ACCESS_KEY" >> ~/.aws/credentials
touch ~/.aws/config
printf "[profile eb-cli]\nregion=us-east-1\noutput=json" >> ~/.aws/config

EB_CONFIG="${PKG}/.elasticbeanstalk/config.${NODE_ENV}.yml"
if [ ! -f "$EB_CONFIG" ] ; then
  EB_CONFIG="${PKG}/.elasticbeanstalk/config.yml"
fi

mkdir -p .elasticbeanstalk
cp -Rf $EB_CONFIG .elasticbeanstalk/config.yml

echo "eb config"
cat .elasticbeanstalk/config.yml

echo "copying docker"
cp $PKG/Dockerfile $PKG/.dockerignore .

# Remove any needed occurrency from gitignore to not be ignored by eb
sed -i '/\/Dockerfile/d' .gitignore
sed -i '/dist/d' $PKG/.gitignore
# Currently Elastic Benstalk does not allow to set ENV variables,
# so we must edit Dockerfile to fix building env
sed -i "s/NODE_ENV=staging/NODE_ENV=${NODE_ENV}/g" Dockerfile

if [ "$APP" = "pc" ]; then
  ## use correct dist for enviroment
  echo "preparing pc dist"
  mv "packages/portal-cliente/dist_${NODE_ENV}" packages/portal-cliente/dist
fi

echo "git status"
git status

export GIT_COMMITTER_NAME="CI"
export GIT_COMMITTER_EMAIL="contato@orsegups.com.br"
git add --all && git commit --author="CI <contato@orsegups.com.br>" -m "chore(deploy): generate deploy files for $ENV"

PACKAGE_VERSION=$(cat package.json \
  | grep version \
  | head -1 \
  | awk -F: '{ print $ENV }' \
  | sed 's/[",]//g')
APP_VERSION="${PACKAGE_VERSION}_${CI_COMMIT_SHORT_SHA}"

APP_VERSION="${APP}_${APP_VERSION}"
APP_VERSION=$(echo -e "${APP_VERSION}" | tr -d '[:space:]')

echo "READY TO DEPLOY"
eb status

echo "deploying ${APP_VERSION} in ${ENV}}"
eb deploy $ENV --label $APP_VERSION --timeout 30
eb setenv APP_VERSION=$APP_VERSION --timeout 30