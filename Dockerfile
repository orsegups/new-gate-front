# THIS DOCKERFILE WILL RUN ON MONOREPO ROOT
FROM nginx:stable
COPY packages/portal-cliente/dist /usr/share/nginx/html
COPY packages/portal-cliente/nginx.conf /etc/nginx/conf.d/default.conf
EXPOSE 80